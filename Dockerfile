FROM python:3.7.5-alpine3.10

RUN apk add -u gcc make musl-dev libffi-dev g++

ADD . /httpbin
WORKDIR /httpbin
RUN pip3 install -r requirements.txt

EXPOSE 5000

ENTRYPOINT ["make", "serve"]
