.PHONY: pip-install lint test serve build release-image deploy

SERVER = 35.194.199.186
HOST_KEY = 35.194.199.186 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBOrb+yJlODPaQ3G01Z7tFs1gz8HEkrYB3D4uwWkgNT2h6IH3DGD92bkivts8QrUmNuBjM23dSrOXQOFzf8gaEus=

DOJO_URL = http://35.194.199.186:8080
CLAIR_ADDR = 35.194.199.186

check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $2, ($2))))

default:
	$(error Please choose a target)

pip-install:
	pip3 install -r requirements.txt

lint:
	flake8 --extend-ignore=W605,E722,E501,F401,F403 httpbin

test:
	python3 test_httpbin.py

sensitive-check:
	trufflehog --max_depth 3 "file://$(PWD)"

sast:
	bandit -lll -ii -r ./httpbin

sast-all:
	#bandit --exit-zero -ii -r -o result -f json ./httpbin/
	bandit -ii -r -o result -f json ./httpbin/ || true

serve:
	gunicorn -b 0.0.0.0:5000 -k gevent httpbin:app

build:
	$(call check_defined, IMAGE_NAME, IMAGE_NAME is not defined)
	docker build -t $(IMAGE_NAME) .
	docker push $(IMAGE_NAME)

zap:
	$(call check_defined, IMAGE_NAME, IMAGE_NAME is not defined)
	docker pull $(IMAGE_NAME)
	docker run --net=host --name service -p 5000:5000 -d $(IMAGE_NAME)
	docker run --net=host --rm -t -v `pwd`:/zap/wrk owasp/zap2docker-weekly zap-baseline.py -t http://127.0.0.1:5000/ -I -x result

release-image:
	$(call check_defined, IMAGE_NAME, IMAGE_NAME is not defined)
	$(call check_defined, RELEASE_IMAGE_NAME, RELEASE_IMAGE_NAME is not defined)
	docker pull $(IMAGE_NAME)
	docker tag $(IMAGE_NAME) $(RELEASE_IMAGE_NAME)
	docker push $(RELEASE_IMAGE_NAME)

deploy:
	$(call check_defined, SSH_KEY, SSH_KEY is not defined)
	$(call check_defined, IMAGE_NAME, IMAGE_NAME is not defined)
	$(call check_defined, IMAGE_ACCESS_TOKEN, IMAGE_ACCESS_TOKEN is not defined)
	mkdir -p $(HOME)/.ssh
	chmod 0600 $(SSH_KEY)
	echo $(HOST_KEY) >> $(HOME)/.ssh/known_hosts
	ssh -i $(SSH_KEY) admin@$(SERVER) " \
		docker login -u Open-Chiang -p $(IMAGE_ACCESS_TOKEN) registry.gitlab.com; \
		docker pull $(IMAGE_NAME); \
		(docker stop service && docker rm service) || true; \
		docker run --name service -p 5000:5000 -d $(IMAGE_NAME); "

upload-result:
	$(call check_defined, DOJO_V2_TOKEN, DOJO_V2_TOKEN is not defined)
	$(call check_defined, SCAN_TYPE, SCAN_TYPE is not defined)
	curl -v $(DOJO_URL)/api/v2/import-scan/ \
		-H 'Authorization: Token $(DOJO_V2_TOKEN)' \
		-H 'Accept-Encoding: gzip, deflate' \
		-F engagement=$(shell curl -H 'Authorization: Token $(DOJO_V2_TOKEN)' $(DOJO_URL)/api/v2/engagements/ | jq '.results | last.id') \
		-F 'scan_type=$(SCAN_TYPE)' \
		-F "file=@result" \
		-F verified=false
